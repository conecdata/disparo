import { PROD } from '../app/modules/_conecdata/_models/consts';

export const environment = {
  production: true,
  idProjeto: 12,
  url: {
    img: {
      tmp: PROD.url.img.tmp,
      conecdata: PROD.url.img.conecdata,
      pedeon: PROD.url.img.pedeon,
      submarine: PROD.url.img.submarine
    },
    api: {
      cdimg: {
        remote: PROD.url.api.cdimg.remote
      },
      cdman: {
        remote: PROD.url.api.cdman.remote
      },
      submarine: {
        remote: PROD.url.api.submarine.remote
      },
    }
  }
};
