import { DEVEL } from '../app/modules/_conecdata/_models/consts';

export const environment = {
  production: false,
  idProjeto: 12,
  url: {
    img: {
      tmp: DEVEL.url.img.tmp,
      conecdata: DEVEL.url.img.conecdata,
      pedeon: DEVEL.url.img.pedeon,
      submarine: DEVEL.url.img.submarine
    },
    api: {
      cdimg: {
        remote: DEVEL.url.api.cdimg.remote
      },
      cdman: {
        remote: DEVEL.url.api.cdman.remote
      },
      submarine: {
        remote: DEVEL.url.api.submarine.remote
      },
    }
  }
};
