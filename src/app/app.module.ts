//#region ng
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router'; 
// import { RouterModule, Router } from '@angular/Router';
import { ServiceWorkerModule } from '@angular/service-worker';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
//#endregion

//#region ionic
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
//#endregion

//#region 3rd
// import { BrMaskerModule } from "br-mask";
import { Ng2BRPipesModule } from 'ng2-brpipes';
// import { TelefonePipe } from "ng2-brpipes";
//#endregion

//#region models
import { environment } from '../environments/environment';
//#endregion

//#region app helpers
import { JwtInterceptor, ErrorInterceptor } from "./helpers";
//#endregion

//#region modules
import { AppRoutingModule } from './app-routing.module';
//#endregion

//#region components
import { AppComponent } from './app.component';
//#endregion

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    Ng2BRPipesModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

// https://blog.jscrambler.com/create-an-ionic-4-pwa-with-capacitor/