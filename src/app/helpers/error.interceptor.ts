//#region ng
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
//#endregion

//#region services
import { StorageService, CdStorageService } from '../services';
//#endregion

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    // constructor(private authenticationService: AuthenticationService) { }

    //#region constructor
    constructor(
        private _storageServ: StorageService,
        private _cdStorageServ: CdStorageService,
        private _router: Router
    ) { }
    //#endregion

    //#region methods
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            console.log(err);
            console.log(err.status);
            if (err.status === 401) {
                console.log('401')
                // this._storageServ.disparoTokenClear();
                // location.reload();
            } // if

            const error = err.error.message || err.statusText;
            return throwError(error);
        }))
    }
    //#endregion
}

// http://jasonwatmore.com/post/2018/05/23/angular-6-jwt-authentication-example-tutorial