//#region ng
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
//#endregion

//#region capacitor
import { Plugins } from '@capacitor/core';
//#endregion

//#region 3rd
import { from, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
// import { fromPromise } from 'rxjs/observable/fromPromise';
//#endregion

//#region app models
const { Storage } = Plugins;
import { environment } from "../../environments/environment";
// import { KEY_DISPARO_TOKEN } from "../modules/_submarine/_models/consts";
//#endregion

//#region services
import { StorageService, CdStorageService } from "../services";
import { KEY_DISPARO_TOKEN } from '../modules/_submarine/_models/consts';
//#endregion

@Injectable({
    providedIn: 'root'
})
export class JwtInterceptor implements HttpInterceptor {
    token = '';
    //#region constructor
    constructor(
        // private _storage: Storage,
        private _storageServ: StorageService,
        private _cdStorageServ: CdStorageService
    ) { }
    //#endregion

    //#region methods
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return from(Storage
            .get({ key: KEY_DISPARO_TOKEN }))
            .pipe(switchMap(token => {
                token = token.value ? JSON.parse(unescape(atob(token.value))) : '';
                if (req.url.includes("/index.php") && !req.url.includes("/send")) {
                    const headers = req.headers
                        .set('Authorization', `Bearer ${token}`)
                    // .set('Access-Control-Allow-Origin', '*')
                    // .append('Content-Type', 'application / json');
                    const reqClone = req.clone({
                        headers
                    });
                    // console.log(reqClone);
                    return next.handle(reqClone);
                } else {
                    return next.handle(req);
                } // else
            }));
        // this._cdStorageServ.get(KEY_DISPARO_TOKEN, (val) => {
        //     this.token = val;
        //     console.log(this.token);
        //     if (req.url.includes("/index.php") && !req.url.includes("/send")) {
        //         req = req.clone({
        //             setHeaders: {
        //                 Authorization: `Bearer ${this.token}`
        //             }
        //         });
        //     } // if
        // })
        // return next.handle(req);
    }
}


    // intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    //     // console.log(req);
    //     // console.log(req.url);
    //     // const KEY_AUTH_TOKEN_PROJ = `${KEY_DISPARO_TOKEN}_${environment.idProjeto}`;
    //     const TOKEN = this._storageServ.disparoTokenGet();
    //     return new Observable((observer) => {

    //     });
    // return from(this._storageServ
    //     .get(KEY_AUTH_TOKEN_PROJ))
    //     .pipe(switchMap(token => {
    //         // console.log(token);
    //         if (req.url.includes("/index.php")) {
    //             const headers = req.headers
    //                 .set('Authorization', `Bearer ${token}`)
    //             // .append('Content-Type', 'application / json');
    //             const reqClone = req.clone({
    //                 headers
    //             });
    //             // console.log(reqClone);
    //             return next.handle(reqClone);
    //         } else {
    //             return next.handle(req);
    //         } // else
    //     }));
    // }
    //#endregion

// http://jasonwatmore.com/post/2018/05/23/angular-6-jwt-authentication-example-tutorial