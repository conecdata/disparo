export interface ISaldo {
    sal_pk: number;
    sal_fk_conta: number;
    sal_fk_produto: number;
    sal_fk_tipo_produto: number;
    sal_i_qtde: number;
    sal_f_val_med_estoque: number;
    sal_c_produto?: string;
    sal_c_icon?: string;
}