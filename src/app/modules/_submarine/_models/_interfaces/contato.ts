//#region models
import { IEndereco } from "../../../_conecdata/_models/_interfaces";
//#endregion

export interface IContato {
    cnt_pk?: number;
    cnt_fk_conta?: number;
    cnt_fk_endereco: number;
    cnt_fk_grupo: number;
    cnt_fk_ult_mov_age: number;
    cnt_b_ativo: boolean;
    cnt_b_problema: boolean;
    cnt_c_contato: string;
    cnt_c_email: string;
    cnt_c_fone: string;
    cnt_c_grupo?: string; // grupos_contatos -> grc_c_grupo
    cnt_d_nascimento: string;
    cnt_e_sexo: string;
    cnt_e_origem: string;
    cnt_o_endereco?: IEndereco;
    sel?: boolean;
}