export interface INotificacao {
    not_pk?: number;
    not_fk_mov_cam: number;
    not_e_tipo: string;
    not_c_campanha: string; // campanha -> cam_c_campanha
    not_c_msg: string;
    not_dt_criado_em: Date;
    not_dt_resolvido_em: Date;
    not_dt_resolvido_em_cdman: Date;
}