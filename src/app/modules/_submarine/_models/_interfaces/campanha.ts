export interface ICampanha {
    cam_pk?: number;
    cam_fk_conta: number;
    cam_fk_produto: number;
    cam_fk_tipo_produto?: number; // tipos_produtos -> tip_pk
    cam_b_alerta1: boolean;
    cam_b_alerta2: boolean;
    cam_b_ilimitado?: boolean; // movimentos_campanhas -> moc_b_ilimitado
    cam_c_campanha: string;
    cam_c_licenca?: string; // contas -> con_c_licenca
    cam_c_produto?: string; // produtos -> pro_c_produto
    cam_c_tipo_grupo_produto?: string;
    cam_c_tipo_produto?: string; // tipos_produtos -> tip_c_tipo
    cam_c_token: string;
    cam_c_uf?: string; // campanhas -> contas -> enderecos -> end_c_uf
    cam_dt_criado_em: Date;
    cam_f_valor: number;
    cam_f_perc_alerta1: number;
    cam_f_perc_alerta2: number;
    cam_i_conta: number;
    perc_disponivel?: number; // movimentos_campanhas -> SUM(moc_i_qtde_disponivel) * 100.0 / SUM(moc_i_qtde_contratada)
    qtde_agendamentos?: number; // agendamentos -> COUNT(DISTINCT(age_pk))
    qtde_movimentos?: number; // movimentos_campanhas -> COUNT(*)
    qtde_itens_pesquisa?: number; // itens_pesquisa -> COUNT(*)
}