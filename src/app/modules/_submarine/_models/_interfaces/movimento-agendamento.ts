export interface IMovimentoAgendamento {
    moa_pk?: number;
    moa_fk_agendamento: number;
    moa_dt_criado_em: Date;
    moa_i_qtde_agendada: number;
    moa_i_qtde_enviada: number;
    moa_f_perc_disponivel: number; // moa_i_qtde_enviada * 100 / moa_i_qtde_agendada
}