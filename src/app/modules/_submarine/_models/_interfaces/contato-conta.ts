export interface IContatoConta {
    idConta: number;
    conta: string;
    licenca: string;
    fone: string;
};