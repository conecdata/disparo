export interface ITipoProdutos {
    tip_pk?: number;
    tip_fk_grp: number;
    tip_fk_produto_default: number;
    tip_c_tipo: string;
}