export interface IAgendamento {
    age_pk?: number;
    age_fk_campanha: number;
    age_fk_conta?: number; // campanhas -> cam_fk_conta
    age_fk_grupo_contatos: number;
    age_b_aniversario: boolean;
    age_b_ativo: boolean;
    age_c_agendamento: string;
    age_c_tipo?: string; //
    age_c_tipo_grupo_produto?: string;
    age_c_tipo_produto?: string; // tipos_produtos -> tip_c_tipo
    age_d_disponivel_em_serv: Date;
    age_e_tipo: string;
    age_i_dia_mes: number;
    age_i_dia_semana: number;
    age_i_hora: number;
    age_i_pre_aniversario: number;
    age_i_qtde: number;
    complemento?: string;
    hora?: string;
}