export interface IStatusMovimento {
    ms_pk?: number;
    ms_fk_movimento: number;
    ms_fk_status: number;
    ms_dt_criado_em: Date;
}