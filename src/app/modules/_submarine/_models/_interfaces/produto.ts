export interface IProduto {
    pro_pk?: number;
    pro_fk_operadora: number;
    pro_fk_tipo: number;
    pro_fk_grp?: number; // tipos_produtos -> tip_fk_grp
    pro_c_api_reply: string;
    pro_c_api_report: string;
    pro_c_nome_operadora: string;
    pro_c_operadora?: string; // operadoras -> ope_c_operadora
    pro_c_tipo?: string; // tipos_produtos -> tip_c_tipo
    pro_e_verbo_http: string;
    pro_f_valor: number;
    pro_i_conta_def: number;
}