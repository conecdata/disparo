export interface IStatus {
    sta_pk?: number;
    sta_c_status: string;
    sta_e_tipo: string;
    sta_c_cod?: string; // ope_sta -> os_c_cod
}