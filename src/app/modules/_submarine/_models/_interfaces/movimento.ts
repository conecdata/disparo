export interface IMovimento {
    mov_pk?: number;
    mov_fk_contato: number;
    mov_fk_mov_age: number;
    mov_fk_mov_campanha: number;
    mov_fk_tipo?: number; // produtos -> pro_fk_tipo
    mov_c_campanha?: string; // campanhas -> cam_c_campanha
    mov_c_extra: string;
    mov_c_message: string;
    mov_c_status?: string; // status -> sta_c_status
    mov_c_subject: string;
    mov_c_uf: string;
    mov_c_to: string;
    mov_dt_criado_em: Date;
    mov_dt_concluido_em: Date;
    mov_dt_schedule: Date;
    mov_e_status_tipo?: string; // status -> sta_e_tipo
    mov_i_id: number;
}