export interface IConta {
    con_pk?: number;
    con_fk_agendamento_ac?: number;
    con_fk_cam_dis_sms?: number;
    con_fk_cam_dis_zap?: number;
    con_fk_campanha_ac?: number;
    con_fk_conta?: number;
    con_fk_endereco: number;
    con_fk_grupo_produto?: number;
    con_b_ativo?: boolean;
    con_b_salvar_contatos_disparo?: boolean;
    con_c_chave_disparo?: string;
    con_c_conta: string;
    con_c_conta_relacionada?: string;
    con_c_cpf_cnpj: string;
    con_c_fone_cel: string;
    con_c_fone_fixo: string;
    con_c_fone_msg: string;
    con_c_email: string;
    con_c_licenca: string;
    con_c_senha: string;
    con_c_uf?: string; // enderecos -> end_c_uf
    total_disparos?: number;
    qtde_disparos?: number;
}