export interface IGrupoContatos {
    grc_pk?: number;
    grc_fk_conta: number;
    grc_c_grupo: string;
    grc_e_origem: string;
    qtde_contatos?: number;
}