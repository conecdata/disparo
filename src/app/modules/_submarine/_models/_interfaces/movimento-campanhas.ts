export interface IMovimentoCampanhas {
    moc_pk?: number;
    moc_fk_campanha: number;
    moc_fk_conta?: number; // campanhas -> cam_fk_conta
    moc_b_alerta1: boolean; // campanhas -> cam_f_perc_alerta1 > moc_f_perc_disponivel
    moc_b_alerta2: boolean; // campanhas -> cam_f_perc_alerta2 > moc_f_perc_disponivel
    moc_b_ilimitado: boolean;
    moc_c_campanha: string; // campanhas -> cam_c_campanha
    moc_dt_criado_em: Date;
    moc_i_qtde_contratada: number;
    moc_i_qtde_creditada: number;
    moc_i_qtde_disponivel: number;
    moc_f_perc_disponivel: number; // moc_i_qtde_disponivel * 100 / moc_i_qtde_contratada
    qtde_itens_pesquisa?: number; // itens_pesquisa -> COUNT(itp_pk)
}