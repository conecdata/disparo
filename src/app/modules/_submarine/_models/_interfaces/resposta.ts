export interface IResposta {
    res_pk?: number;
    res_fk_movimento: number;
    res_fk_tipo?: number; // produtos -> pro_fk_tipo
    res_c_campanha?: string; // campanhas -> cam_c_campanha
    res_c_extra: string;
    res_c_from: string;
    res_c_message: string;
    res_dt_criado_em: Date;
    res_dt_ult_reportado_serv: Date;
    res_i_id?: number; // movimentos -> mov_i_id
}