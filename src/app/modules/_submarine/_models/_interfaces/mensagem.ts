export interface IMensagem {
    msg_pk?: number;
    msg_fk_agendamento?: number;
    msg_fk_campanha?: number;
    msg_fk_grupo_produtos?: number;
    msg_b_old?: boolean;
    msg_c_arq?: string;
    msg_c_arq_id?: string;
    msg_c_extra?: string;
    msg_c_id?: string;
    msg_c_msg: string;
    fileData?: File;
}