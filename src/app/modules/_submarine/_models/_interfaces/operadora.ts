export interface IOperadora {
    ope_pk?: number;
    ope_c_operadora: string;
    ope_c_label: string;
}