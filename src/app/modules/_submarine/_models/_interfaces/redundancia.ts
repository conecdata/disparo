export interface IRedundancia {
    red_pk?: number;
    red_fk_produto: number;
    red_b_estepe: boolean;
    red_b_padrao: boolean;
    red_c_api_msg: string;
    red_c_chave: string;
    red_c_fone: string;
    red_c_redundancia: string;
    red_c_conta?: string;
    red_c_produto?: string;
    red_dt_criado_em: Date;
    red_dt_desativada_em: Date;
    qtde_contas?: number;
    qtde_disparos?: number;
}