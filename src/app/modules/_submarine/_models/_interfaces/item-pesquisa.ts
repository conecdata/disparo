export interface IItemPesquisa {
    itp_pk?: number;
    itp_fk_campanha: number;
    itp_c_item: string;
}