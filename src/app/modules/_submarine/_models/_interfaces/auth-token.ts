export interface IAuthToken {
    cdman?: boolean;
    idCliente?: number;
    nome?: string;
    licenca?: string;
}