// Storage
// Submarine dashboard
export const KEY_AUTH_TOKEN: string = "KEY_AUTH_TOKEN_4";
export const KEY_PAG_CAM: string = "KEY_PAG_CAM_4";
export const KEY_PAG_CNT: string = "KEY_PAG_CNT_4";
export const KEY_PAG_CON: string = "KEY_PAG_CON_4";
export const KEY_PAG_GRP: string = "KEY_PAG_GRP_4";
export const KEY_PAG_MOV: string = "KEY_PAG_MOV_4";
export const KEY_PAG_NOT: string = "KEY_PAG_NOT_4";
export const KEY_PAG_OPE: string = "KEY_PAG_OPE_4";
export const KEY_PAG_PRO: string = "KEY_PAG_PRO_4";
export const KEY_PAG_RED: string = "KEY_PAG_RED_4";
export const KEY_PAG_RES: string = "KEY_PAG_RES_4";
export const KEY_PAG_STA: string = "KEY_PAG_STA_4";
export const KEY_PAG_TPR: string = "KEY_PAG_TPR_4";
export const KEY_EMAIL_LOGIN: string = "KEY_EMAIL_LOGIN_4";
// Submarine contatos
export const KEY_CONTATO_TOKEN: string = "KEY_CONTATO_TOKEN_12";
export const KEY_CONTATO_CONTA: string = "KEY_CONTATO_CONTA_12";
// Submarine disparo
export const KEY_DISPARO_TOKEN: string = "KEY_DISPARO_TOKEN_13";

// Tipos produtos
export const TIPO_EMAIL: number = 1;
export const TIPO_SMS: number = 2;
export const TIPO_WHATSAPP: number = 3;
// Ícones tipos produtos
const TIPOS_PRODUTOS_ICONS: string[] = [];
TIPOS_PRODUTOS_ICONS[TIPO_EMAIL] = 'icon-at';
TIPOS_PRODUTOS_ICONS[TIPO_SMS] = 'icon-sms';
TIPOS_PRODUTOS_ICONS[TIPO_WHATSAPP] = 'icon-whatsapp';
export { TIPOS_PRODUTOS_ICONS };