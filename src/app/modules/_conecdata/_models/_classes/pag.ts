//#region 3rd
import { PaginationInstance } from 'ngx-pagination/dist/pagination-instance';
//#endregion

//#region app models
import { IPagination } from '../_interfaces';
//#endregion

//#region app services
import { PagService } from '../../_core/_services';
//#endregion

export class Pag {
    //#region publics
    filtered: boolean = false;
    //#endregion

    //#region privates
    private _pag: IPagination;
    private _pagInst: PaginationInstance;
    private _constructing: boolean;
    //#endregion

    //#region constructor
    constructor(
        pag: IPagination = null
    ) {
        this._constructing = true;
        this._pag = pag;
        if (!this._pag) {
            this._pag = {
                filter: '',
                current: 1,
                total: 0,
                limit: 10,
                sorting: {
                    field: '',
                    desc: false
                }
            };
        } // if

        this._pagInst = {
            currentPage: this._pag.current,
            itemsPerPage: parseInt(this._pag.limit.toString()),
            totalItems: this._pag.total
        };
        this._constructing = false;
        // this._changed();
    }
    //#endregion

    //#region methods
    public getPag(): IPagination {
        return this._pag;
    }

    public getPagInst(): PaginationInstance {
        return this._pagInst;
    }

    public setCurrent(page: number, emitChanged: boolean = true) {
        // console.log(`setCurrent: ${page}, ${emitChanged}`);
        this._pagInst.currentPage = page;
        this._pag.current = page;
        if (emitChanged) {
            this._changed();
        } // if
    }
    public getCurrent(): number {
        return this._pag.current;
    }

    public setFilter(criteria: string, emitChanged: boolean = true) {
        // console.log(`setFilter: ${criteria}`);
        this._pag.filter = criteria.trim();
        this.filtered = this._pag.filter != '';
        if (emitChanged) {
            this._changed();
        } // if
    }
    public getFilter(): string {
        return this._pag.filter;
    }

    public setLimit(s: string, emitChanged: boolean = true) {
        // console.log(`setLimit: ${s}`);
        this._pagInst.itemsPerPage = parseInt(s);
        this._pag.limit = s;
        if (emitChanged) {
            this._changed();
        } // if
    }
    public getLimit(): string {
        return this._pag.limit.toString();
    }

    public setSortingField(fld: string, emitChanged: boolean = true) {
        // console.log(`setSortingField: ${fld}`);
        if (this._pag) {
            if (fld == this._pag.sorting.field) {
                this._pag.sorting.desc = !this._pag.sorting.desc;
            } else {
                this._pag.sorting.field = fld;
                this._pag.sorting.desc = false;
            }; // else
            // console.log(`setSortingField: ${fld} ${this._pag.sorting.field} ${this._pag.sorting.desc}`);

            this._pag.sorting.field = fld;
            if (emitChanged) {
                this._changed();
            } // if
        } // if
    }
    public getSortingField() {
        if (this._pag && this._pag.sorting) {
            return this._pag.sorting.field;
        } else {
            return '';
        } // else
    }
    public getSortingDesc() {
        if (this._pag && this._pag.sorting) {
            return this._pag.sorting.desc;
        } else {
            return false;
        } // else
    }

    public setTotal(tot: number) {
        // console.log(`setTotal: ${tot}`);
        if (this._pag) {
            this._pagInst.totalItems = tot;
        } // if
        if (this._pag) {
            this._pag.total = tot;
        } // if
        // this._changed();
    }
    public getTotal(): number {
        return this._pag.total;
    }

    private _changed() {
        if (!this._constructing) {
            PagService.onPagChangedEvent.emit(this._pag);
        } // if
    }
    //#endregion
}
