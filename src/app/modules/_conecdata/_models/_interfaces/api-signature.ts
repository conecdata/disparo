export interface IApiSignature {
  local: any;
  remote: any;
  id: any;
  app: string;
}
