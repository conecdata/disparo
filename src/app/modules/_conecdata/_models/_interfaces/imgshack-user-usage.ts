export interface IImgshackUserUsage {
    images_count: number;
    album_images_count: number;
    albums_count: number;
    root_images_count: number;
    space_limit: number;
    space_used: number;
}