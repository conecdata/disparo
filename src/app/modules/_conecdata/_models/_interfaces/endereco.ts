export interface IEndereco {
  end_pk: number;
  end_b_favorito?: boolean;
  end_c_apelido?: string;
  end_c_bairro: string;
  end_c_cep: string;
  end_c_complemento: string;
  end_c_localidade: string;
  end_c_logradouro: string;
  end_c_referencia: string;
  end_c_uf?: string;
  end_i_nro: number;
}
