export interface IAuth {
  cod?: number;
  email?: string;
  usuario?: string;
  senha: string;
  conectado?: boolean;
}
