export interface IImgshackImageList {
    base_url: string;
    limit: number;
    offset: number;
    total: number;
    images: {
        id: string;
        server: number;
        bucket: number;
        filename: string;
        original_filename: string;
        title: string;
        creation_date: number;
        public: boolean;
        hidden: boolean;
        filesize: number;
        width: number;
        height: number;
        is_owner: boolean;
        owner: {
            username: string;
            avatar: {
                id: string;
                server: number;
                filename: string;
            };
        };
    }[];
}