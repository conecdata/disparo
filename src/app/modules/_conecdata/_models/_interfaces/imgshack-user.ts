export interface IImgshackUser {
    is_owner: boolean;
    cache_version: number;
    userid: number;
    email: string;
    username: string;
    description: string;
    creation_date: number;
    location: string;
    gender: string;
    first_name: string;
    last_name: string;
    avatar: {
        id: string;
        server: number;
        filename: string;
    },
    membership: string;
    is_following: boolean;
    following_allowed: boolean;
    latest_images: [];
}