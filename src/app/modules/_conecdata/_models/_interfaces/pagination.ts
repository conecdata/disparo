export interface IPagination {
    // extras?: any[];
    id?: string;
    filter: string;
    current: number;
    total: number;
    limit: string | number;
    sorting?: {
        field: string;
        desc: boolean
    };
}
