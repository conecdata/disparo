// Strings
export const FORM_CORRIJA_CAMPOS: string = "Corrija valores indicados.";

// Grid System
export const GRID_WIDTH_SM: number = 540;
export const GRID_WIDTH_MD: number = 720;
export const GRID_WIDTH_LG: number = 960;
export const GRID_WIDTH_XL: number = 1140;

// Pagination
export const PAG_ITENS_POR_PAG = [
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    30,
    35,
    40,
    45,
    50,
    55,
    60,
    65,
    70,
    75,
    80,
    85,
    90,
    95,
    100
];

export const DIA_SEM: string[] = [
    'Dom',
    'Seg',
    'Ter',
    'Qua',
    'Qui',
    'Sex',
    'Sab'
];

// Environments references
export const DEVEL = {
    url: {
        img: {
            tmp: "http://192.168.200.155/tmp", // "http://54.39.245.19/tmp"
            conecdata: "http://54.39.245.19/assets/img/conecdata",
            pedeon: "http://54.39.245.19/assets/img/pedeon",
            submarine: "http://54.39.245.19/assets/img/submarine"
        },
        api: {
            cdimg: {
                remote: {
                    root: "http://192.168.200.155/ws/cdimg/remote/v1/index.php",
                    conecdata: {
                        produtos: "http://54.39.245.19/ws/cdimg/remote/v1/index.php/conecdata/produtos"
                    }
                }
            },
            cdman: {
                remote: "http://192.168.200.155/ws/cdman/v1/index.php"
            },
            frigolabel: {
                local:
                    "http://192.168.200.155/ws/frigolabel/local/v1/index.php",
                remote:
                    "http://192.168.200.155/ws/frigolabel/remote/v1/index.php"
            },
            submarine: {
                remote:
                    "http://192.168.200.155/ws/submarine/v4/index.php"
            },
            connpbx: {
                remote:
                    "http://192.168.200.155/ws/connpbx/v1/index.php"
            },
            pedeon: {
                ope: {
                    local:
                        "http://192.168.200.155/ws/pedeon/ope/local/v1/index.php",
                    remote:
                        "http://192.168.200.155/ws/pedeon/ope/remote/v1/index.php"
                },
                con: {
                    remote:
                        "http://192.168.200.155/ws/pedeon/con/v1/index.php"
                }
            }
        }
    }
};

export const PROD = {
    url: {
        img: {
            tmp: "http://54.39.245.19/tmp",
            conecdata: "http://54.39.245.19/assets/img/conecdata",
            pedeon: "http://54.39.245.19/assets/img/pedeon",
            submarine: "http://54.39.245.19/assets/img/submarine"
        },
        api: {
            cdimg: {
                remote: {
                    root: "http://54.39.245.19/ws/cdimg/remote/v1/index.php",
                    conecdata: {
                        produtos: "http://54.39.245.19/ws/cdimg/remote/v1/index.php/conecdata/produtos"
                    }
                }
            },
            cdman: {
                remote: "http://54.39.245.19/ws/cdman/v1/index.php"
            },
            frigolabel: {
                local:
                    "http://54.39.245.19/ws/frigolabel/local/v1/index.php",
                remote:
                    "http://54.39.245.19/ws/frigolabel/remote/v1/index.php"
            },
            submarine: {
                remote:
                    "http://54.39.245.19/ws/submarine/v4/index.php"
            },
            connpbx: {
                remote:
                    "http://192.168.200.155/ws/connpbx/v1/index.php"
            },
            pedeon: {
                ope: {
                    local:
                        "http://54.39.245.19/ws/pedeon/ope/local/v1/index.php",
                    remote:
                        "http://54.39.245.19/ws/pedeon/ope/remote/v1/index.php"
                },
                con: {
                    remote:
                        "http://54.39.245.19/ws/pedeon/con/v1/index.php"
                }
            }
        }
    }
};

