//#region ng
import { Pipe, PipeTransform } from '@angular/core';
//#endregion

@Pipe({ name: 'licenca' })
export class LicencaPipe implements PipeTransform {
    transform(value: string): string {
        if (value && value.length == 8) {
            return value[0] + '-' + value.slice(1)
        } else {
            return '';
        } // else
        /* 
        let newStr: string = "";
        for (var i = value.length - 1; i >= 0; i--) {
            newStr += value.charAt(i);
        } // for
        return newStr;
       */
    }
}