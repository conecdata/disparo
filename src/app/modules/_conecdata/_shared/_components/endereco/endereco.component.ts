//#region ng
import {Component, Input} from "@angular/core";
//#endregion

//#region models
import { IEndereco } from "../../../_models/_interfaces";
//#endregion

@Component({
  selector: "cd-endereco",
  templateUrl: "endereco.component.html",
  styleUrls: ["endereco.component.scss"]
})
export class EnderecoComponent {
  //#region comm
  @Input() endereco: IEndereco;
  //#endregion
}
