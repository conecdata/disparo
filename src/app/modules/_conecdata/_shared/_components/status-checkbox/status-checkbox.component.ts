//#region ng
import {Component, Input} from "@angular/core";
//#endregion

@Component({
  selector: "cd-status-checkbox",
  templateUrl: "status-checkbox.component.html",
  styleUrls: ["status-checkbox.component.scss"]
})
export class StatusCheckboxComponent {
  //#region comm
  @Input() status: boolean;
  //#endregion
}
