//#region ng
import {
    Directive,
    ElementRef,
    HostListener,
    Input,
} from '@angular/core';
import { NgForm, FormControl } from '@angular/forms';
//#endregion

@Directive({
    selector: '[onReturn]'
})
export class OnReturnDirective {

    //#region comm
    @Input() onReturn: NgForm;
    //#endregion

    //#region constructor
    constructor(private _el: ElementRef) { }
    //#endregion

    //#region body
    @HostListener('window:keyup', ['$event'])
    keyEvent(event: KeyboardEvent) {
        // console.log(this.onReturn);
        // console.log(event);
        if (event.keyCode === 13) {
            // console.log('return pressed');
            event.preventDefault();

            // // Busca controle atual do Form
            // console.log(this._el);
            // console.log(this._el.nativeElement);
            // console.log(this._el.nativeElement.className);
            // let currentControlName: string;
            // if (
            //     this._el
            //     && this._el.nativeElement
            //     && this._el.nativeElement.attributes
            // ) {
            //     Object.keys(this._el.nativeElement.attributes).forEach(key => {
            //         // focusable.controls[key].markAsDirty();

            //         // console.log(this._el.nativeElement.attributes[key].indexOf('formcontrolname="'));
            //         console.log(this._el.nativeElement.attributes[key]);
            //         // focusable.controls[key].focus();
            //         // i++;
            //         // if (key)
            //     });
            // } // if

            let focusable: any;
            // let control: any;
            // control = this._el.nativeElement;
            // console.log(control);
            if (this.onReturn) {
                focusable = this.onReturn.form.controls || {};
                // console.log(focusable);
                if (focusable) {
                    // console.log(focusable);
                    // console.log(event.target);
                    // var currentIndex = Array.prototype.indexOf.call(focusable, event.target);
                    // let currentIndex = focusable.filter((e) => e.name == event.target.name);
                    // console.log('currentIndex', currentIndex);
                    let i = 0;
                    Object.keys(focusable).forEach(key => {
                        // focusable.controls[key].markAsDirty();
                        // console.log(focusable[key]);
                        // focusable[key].focus();
                        // i++;
                        // if (key)
                    });

                    // var nextIndex = currentIndex == focusable.length - 1 ? 0 : currentIndex + 1;
                    // if (nextIndex >= 0 && nextIndex < focusable.length)
                    //     focusable[nextIndex].focus();


                }; // if

                /* while (true) {
                    if (control) {
                        if ((!control.hidden) &&
                            (control.nodeName == 'INPUT' ||
                                control.nodeName == 'SELECT' ||
                                control.nodeName == 'BUTTON' ||
                                control.nodeName == 'TEXTAREA')) {
                            control.focus();
                            return;
                        } else {
                            control = control.nextElementSibling;
                        }
                    }
                    else {
                        console.log('close keyboard');
                        return;
                    }
                } // while */
            } // if
        } // if
    }
    /*
    @HostListener('keydown', ['$event']) onKeyDown(e) {
        console.log(e);
        if (e.which == 13 || e.keyCode == 13) {
            e.preventDefault();
            var focusable;
            if (this._el
                && this._el.nativeElement
                && this._el.nativeElement.form) {
                focusable = this._el.nativeElement.form.querySelectorAll('input,select,button,textarea');
            }; // if

            if (focusable) {
                // console.log(focusable);
                var currentIndex = Array.prototype.indexOf.call(focusable, e.target)
                var nextIndex = currentIndex == focusable.length - 1 ? 0 : currentIndex + 1;
                if (nextIndex >= 0 && nextIndex < focusable.length)
                    focusable[nextIndex].focus();
            }; // if
            return;
        }; // if
    }
    */
    //#endregion
}
