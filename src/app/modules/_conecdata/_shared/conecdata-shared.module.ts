//#region ng
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule } from '@angular/forms';
//#endregion

//#region ionic
// import { IonicModule } from "@ionic/angular";
//#endregion

//#region 3rd
import { Ng2BRPipesModule } from 'ng2-brpipes';
//#endregion

//#region app components
import { EnderecoComponent, StatusCheckboxComponent } from "./_components";
//#endregion

//#region app directives
import {
  FocusDirective,
  SelectDirective,
  OnReturnDirective,
} from "./_directives";
//#endregion

//#region app pipes
import { LicencaPipe } from "./_pipes";
//#endregion

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    // IonicModule,
    FormsModule,
    Ng2BRPipesModule
  ],
  declarations: [
    // components
    EnderecoComponent,
    StatusCheckboxComponent,
    // directives
    FocusDirective,
    SelectDirective,
    OnReturnDirective,
    // pipes
    LicencaPipe
  ],
  exports: [
    // components
    EnderecoComponent,
    StatusCheckboxComponent,
    // directives
    FocusDirective,
    SelectDirective,
    OnReturnDirective,
    // pipes
    LicencaPipe
  ]

})
export class ConecdataSharedModule { }
