//#region ng
import {
    EventEmitter,
    Injectable,
} from '@angular/core';
//#endregion

//#region app models
import { IPagination } from '../../_models/_interfaces';
//#endregion

@Injectable({
    providedIn: "root"
})
export class PagService {
    //#region pagination
    static onPagChangedEvent: EventEmitter<IPagination> = new EventEmitter<IPagination>();
    static onPagTotalChangedEvent: EventEmitter<number> = new EventEmitter<number>();
    //#endregion
}
