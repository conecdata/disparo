//#region ng
import { Injectable } from "@angular/core";
//#endregion

@Injectable({
  providedIn: "root"
})
export class UtilsService {
  /*
   * Generates a GUID string.
   * @returns {String} The generated GUID.
   * @example af8a8416-6e18-a307-bd9c-f2c947bbb3aa
   * @author Slavik Meltser (slavik@meltser.info).
   * @link http://slavik.meltser.info/?p=142
   */
  private _p8(s: boolean = false) {
    var p = (Math.random().toString(16) + "000000000").substr(2, 8);
    return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
  }

  guid(): string {
    return this._p8() + this._p8(true) + this._p8(true) + this._p8();
  }

  checkImg(imgSrc: string, onSuccess, onError = null) {
    let img = new Image();
    if (onSuccess && typeof onSuccess === "function") {
      img.onload = onSuccess;
    } // if 
    if (onError && typeof onError === "function") {
      img.onerror = onError;
    } // if
    img.src = imgSrc;
  }

  copyToClipboard(text) {
    var dummy = document.createElement("textarea");
    // to avoid breaking orgain page when copying more words
    // cant copy when adding below this code
    // dummy.style.display = 'none'
    document.body.appendChild(dummy);
    //Be careful if you use texarea. setAttribute('value', value), which works with "input" does not work with "textarea". – Eduard
    dummy.value = text;
    dummy.select();
    document.execCommand("copy");
    document.body.removeChild(dummy);
  }
}
