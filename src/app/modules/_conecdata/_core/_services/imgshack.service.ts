//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region models
// const IMG_SHACK_BASE_API: string = 'https://api.imageshack.com/v2/';
// const IMG_SHACK_USER: string = 'conecdata';
import { environment } from "../../../../../environments/environment";
// import { IMensagem } from "../../../_submarine/_models/_interfaces";
//#endregion

@Injectable({
    providedIn: "root"
})
export class ImgShackService {
    //#region constructor
    constructor(
        private _http: HttpClient
    ) { }
    //#endregion

    //#region misc
    // fix(row: any): IProduto {
    //     return this._produtosServ.fix(row);
    // }

    // fixes(rows: IProduto[]): IProduto[] {
    //     return this._produtosServ.fixes(rows);
    // }

    // img(produto: IProduto, big: boolean = false) {
    //     return this._produtosServ.img(produto, big);
    // }
    //#endregion

    //#region methods
    getUser(): Observable<any> {
        const URL = encodeURI(`${environment.url.api.cdimg.remote.root}/imageshack/user`);
        // const URL = encodeURI(`${IMG_SHACK_BASE_API}user/${IMG_SHACK_USER}`);
        // console.log(`url: ${URL}`);
        return this._http.get(URL);
        // const URL = "../../../../../assets/mocks/imgShack-getUser.json";
        // return this._http.get(URL);
    }

    getUserUsage(): Observable<any> {
        const URL = encodeURI(`${environment.url.api.cdimg.remote.root}/imageshack/user-usage`);
        // const URL = encodeURI(`${IMG_SHACK_BASE_API}user/${IMG_SHACK_USER}/usage`);
        // console.log(`url: ${URL}`);
        return this._http.get(URL);
        // const URL = "../../../../../assets/mocks/imgShack-getUserUsage.json";
        // return this._http.get(URL);
    }

    getImages(): Observable<any> {
        // https://api.imageshack.com/v2/user/conecdata/images
        const URL = encodeURI(`${environment.url.api.cdimg.remote.root}/imageshack/images`);
        // const URL = encodeURI(`${IMG_SHACK_BASE_API}user/${IMG_SHACK_USER}/images`);
        // console.log(`url: ${URL}`);
        return this._http.get(URL);
        // const URL = "../../../../../assets/mocks/imgShack-getImages.json";
        // return this._http.get(URL);
    }
    //#endregion
    /*
    //#region submarine methods
    subUploadMsgFile(msg: IMensagem, licenca: string = ''): Observable<any> {
        const INPUT = new FormData();
        if (msg.fileData) {
            INPUT.append('file', msg.fileData, msg.fileData.name);
        }
        INPUT.append('msg', JSON.stringify(msg));
        INPUT.append('licenca', licenca);
        const URL = encodeURI(`${environment.url.api.cdimg.remote.root}/submarine/upload-msg`);
        // console.log(`url: ${URL}`);
        return this._http.post(URL, INPUT, {
            reportProgress: true,
            observe: 'events'
        });
    }
    //#endregion
    */
}