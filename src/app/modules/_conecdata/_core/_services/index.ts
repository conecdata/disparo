export * from './api.service';
export * from './cep.service';
export * from './cdimg.service';
export * from './imgshack.service';
export * from './pag.service';
export * from './storage.service';
export * from './utils.service';