//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

@Injectable({
  providedIn: "root"
})
export class ApiService {
  //#region constructor
  constructor(private _http: HttpClient) { }
  //#endregion

  apiGet(url: string): Observable<any> {
    // const URL = encodeURI(`${this.VIA_CEP_API}/${cep}/json/`);
    return this._http.get(url);
  }
}
