//#region angular
import { Injectable } from '@angular/core';
//#endregion

// import { Plugins } from '@capacitor/core';
// const { Storage } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  //#region s
  _wsOk: boolean = false;
  _log: boolean = false;
  //#endregion

  //#region constructor
  constructor() {
    this._wsOk = typeof (Storage) !== 'undefined';
  }
  //#endregion

  //#region sessionStorage
  ss_get(
    key: string,
    defaultValue: any
  ): any {
    let a: any;
    if (this._wsOk) {
      a = sessionStorage.getItem(key);
      if (a === null) {
        if (this._log) console.log(`ss_get.${key}: ${a}`);
        return defaultValue;
      } else {
        a = JSON.parse(unescape(atob(a)));
        if (this._log) console.log(`ss_get.${key}: ${a}`);
        return a;
      } // else
    } else {
      return defaultValue;
    }; // else
  }

  ss_set(
    key: string,
    value: any
  ): boolean {
    value = btoa(escape(JSON.stringify(value)));
    if (this._wsOk) {
      sessionStorage.setItem(key, value);
      if (this._log) console.log(`ss_set.${key}: ${value}`);
      return true;
    } else {
      return false;
    }; // else
  }

  ss_remove(
    key: string
  ): boolean {
    if (this._wsOk) {
      sessionStorage.removeItem(key);
      if (this._log) console.log(`ss_remove.${key}`);
      return true;
    } else {
      return false;
    }; // else
  }
  //#endregion

  //#region localStorage
  ls_get(
    key: string,
    defaultValue: any
  ): any {
    let a: any;
    if (this._wsOk) {
      a = localStorage.getItem(key);
      if (a === null) {
        if (this._log) console.log(`ls_get.${key}: ${a}`);
        return defaultValue;
      } else {
        a = JSON.parse(unescape(atob(a)));
        if (this._log) console.log(`ls_get.${key}: ${a}`);
        return a;
      } // else
    } else {
      return defaultValue;
    }; // else
  }

  ls_set(
    key: string,
    value: any
  ): boolean {
    value = btoa(escape(JSON.stringify(value)));
    if (this._wsOk) {
      localStorage.setItem(key, value);
      if (this._log) console.log(`ls_set.${key}: ${value}`);
      return true;
    } else {
      return false;
    }; // else
  }

  ls_remove(
    key: string
  ): boolean {
    if (this._wsOk) {
      localStorage.removeItem(key);
      if (this._log) console.log(`ls_remove.${key}`);
      return true;
    } else {
      return false;
    }; // else
  }
  //#endregion
}