//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region models
import { environment } from "../../../../../environments/environment";
// import { IMensagem } from "../../../_submarine/_models/_interfaces";
//#endregion

@Injectable({
    providedIn: "root"
})
export class CdImgService {
    //#region constructor
    constructor(
        private _http: HttpClient
    ) { }
    //#endregion

    //#region misc
    // fix(row: any): IProduto {
    //     return this._produtosServ.fix(row);
    // }

    // fixes(rows: IProduto[]): IProduto[] {
    //     return this._produtosServ.fixes(rows);
    // }

    // img(produto: IProduto, big: boolean = false) {
    //     return this._produtosServ.img(produto, big);
    // }
    //#endregion

    //#region methods
    formasPgto(filter: string): Observable<any> {
        const INPUT = {
            filter: filter
        };
        const URL = encodeURI(`${environment.url.api.cdimg.remote.root}/conecdata/formas-pgto`);
        // console.log(`url: ${URL}`);
        return this._http.post(URL, INPUT);
    }
    //#endregion
    /* 
        //#region submarine methods
        subUploadMsgFile(msg: IMensagem, licenca: string = ''): Observable<any> {
            const INPUT = new FormData();
            if (msg.fileData) {
                INPUT.append('file', msg.fileData, msg.fileData.name);
            }
            INPUT.append('msg', JSON.stringify(msg));
            INPUT.append('licenca', licenca);
            const URL = encodeURI(`${environment.url.api.cdimg.remote.root}/submarine/upload-msg`);
            // console.log(`url: ${URL}`);
            return this._http.post(URL, INPUT, {
                reportProgress: true,
                observe: 'events'
            });
        }
        //#endregion 
    */
}