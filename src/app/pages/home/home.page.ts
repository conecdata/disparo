//#region ng
import { Component, EventEmitter, OnInit } from '@angular/core';
//#endregion

//#region ionic
import { LoadingController } from "@ionic/angular";
//#region

//#region models
// import { environment } from '../../../environments/environment';
import { KEY_DISPARO_TOKEN } from '../../modules/_submarine/_models/consts'
import { IApiResponse } from '../../modules/_conecdata/_models/_interfaces'
//#endregion

//#region service
import { StorageService, ContasService, CampanhasService, MensagensService, ApiService, CdStorageService } from '../../services';
import { mergeMap, map } from 'rxjs/operators';
//#endregion

//#region 3rd
// import { Ng2BRPipesModule } from "ng2-brpipes";
//#endregion

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  //#region comm
  onCelFocusEvent: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  //#endregion

  //#region publics
  busy: boolean = false;
  display: string = '';
  token: string;
  conta: any;
  numProds: number;
  //#endregion

  //#region buffer
  private _buffer: string = '';
  set buffer(val: string) {
    this._buffer = val ? val.replace(/\D/g, '').substring(0, 11) : '';
    this.display = this._buffer.padEnd(11, '_');
  }
  get buffer(): string {
    return this._buffer;
  }
  //#endregion

  //#region constructor
  constructor(
    private _loadingCtrl: LoadingController,
    private _campanhasServ: CampanhasService,
    private _contasServ: ContasService,
    private _mensagensServ: MensagensService,
    // private _storageServ: StorageService,
    private _cdStorageServ: CdStorageService,
    private _apiServ: ApiService
  ) { }
  //#endregion

  //#region lifecycles
  ngOnInit() {
    this._refresh();
  }
  //#endregion

  //#region functions
  private _refresh() {
    this._cdStorageServ.get(KEY_DISPARO_TOKEN,
      (token) => {
        console.log(token);
        this.token = token;
        if (this.token) {
          this._contasServ.validarToken(this.token).subscribe(
            (api: IApiResponse) => {
              console.log(api);
              if (api.ok) {
                if (api.data) {
                  this.conta = api.data;
                  // console.log(this.conta);
                  // this.conta.con_fk_cam_dis_zap = 123;
                  // this.conta.con_fk_cam_dis_sms = 123;
                  this.numProds = 0;
                  if (this.conta.con_fk_cam_dis_zap) {
                    this.numProds++;
                  } // if
                  if (this.conta.con_fk_cam_dis_sms) {
                    this.numProds++;
                  } // if
                  // console.log(this.conta);
                  this.onClearClick();
                } else {
                  this._cdStorageServ.removeStorageItem(KEY_DISPARO_TOKEN);
                  // this._refresh();
                  // alert('Chave inválida.');
                } // else
              } else {
                console.error(JSON.stringify(api.errors));
              } // else
            }
          );
        } // if
      }
    );
  }
  //#endregion

  //#region methods
  addDigitClick(digit: string) {
    this.buffer += digit;
  }

  onClearClick() {
    this.busy = false;
    if (this.token) {
      // console.log(this.conta);
      this.buffer = this.conta.con_c_fone_msg.substring(0, 2);
    } else {
      this.buffer = '';
    } // else
  }

  onBackspaceClick() {
    if (this.buffer) {
      this.buffer = this.buffer.slice(0, -1);
    } // if
    // this.buffer.substring(0, this.buffer.length - 1);
  }

  async onRegistrarClick() {
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });
    loading.present().then(() => {
      this._contasServ.registrar(this.buffer).subscribe(
        (api: IApiResponse) => {
          // console.log(api);
          if (api.ok) {
            loading.dismiss();
            // console.log(api);
            if (api.data) {
              this._cdStorageServ.store(KEY_DISPARO_TOKEN, api.data);
              // this._storageServ.disparoTokenSet(api.data);
              this._refresh();
            } else {
              loading.dismiss();
              alert('Chave inválida.');
              this.onClearClick();
            } // else
          } else {
            loading.dismiss();
            console.error(JSON.stringify(api.errors));
          } // else
        }
      );

    }) // loading
  }

  async onWhatsappSend() {
    this.busy = true;
    // console.log(this.conta);
    // console.log(this.buffer);
    // console.log(this.token);
    // console.log(this.conta.con_fk_cam_dis_zap);
    let loading = await this._loadingCtrl.create({
      // message: "Aguarde...",
      translucent: true
    });

    loading.present().then(() => {
      console.log(this.conta);
      // if (this.token == this.conta.con_c_chave_licenca) {
      this._campanhasServ.campanha(this.conta.con_fk_cam_dis_zap)
        .pipe(
          mergeMap((campanha: IApiResponse) => {
            // console.log(campanha);
            // return this._mensagensServ.buscaCampanha(campanha.data.cam_pk, campanha.data.cam_fk_conta)
            return this._mensagensServ.buscaCampanha(this.conta.con_fk_cam_dis_zap, this.conta.con_pk, this.conta.con_c_chave_disparo)
              .pipe(
                map((mensagens: IApiResponse) => {
                  // console.log(mensagens);
                  return {
                    campanha,
                    mensagens
                  }
                })
              )
          })
        )
        .subscribe(
          (api: any) => {
            // if (this._contasServ.validarToken(this.token) == this.conta.con_c_chave_licenca) {
            console.log(api);
            if (api.mensagens.ok) {
              // console.log(api.campanha.data.cam_c_token);
              // console.log(this.buffer);
              // console.log(api.mensagens.data);
              this._apiServ.send(
                api.campanha.data.cam_c_token,
                this.buffer,
                api.mensagens.data,
              ).subscribe(
                (api: IApiResponse) => {
                  // console.log(api);
                  this.busy = false;
                  if (api.ok) {
                    loading.dismiss();
                    this.onClearClick();
                  } else {
                    loading.dismiss();
                    this._refresh();
                    console.error(JSON.stringify(api.errors));
                  } // else
                })
            } else {
              this._refresh();
              this._cdStorageServ.removeStorageItem(KEY_DISPARO_TOKEN);
              loading.dismiss();
              console.error(JSON.stringify(api.mensagens.errors));
            } // if
            // } else {
            // this._refresh();
            // this._cdStorageServ.removeStorageItem(KEY_DISPARO_TOKEN);
            // } // if
          },
          err => {
            loading.dismiss();
            console.error(err);
          }
        );
      // } else {
      //   loading.dismiss();
      //   this.onClearClick();
      //   // this._cdStorageServ.removeStorageItem(KEY_DISPARO_TOKEN);
      //   this._refresh();
      // }

    }) // loading

  }

  onSmsSend() {
    console.log('SMS');
  }

  img(): string {
    // console.log(this.conta.licenca);
    const IMG = this._contasServ.img(this.conta.con_c_licenca);
    // console.log(IMG);
    return IMG;
  }

  //#endregion
}

// this.token = this._storageServ.disparoTokenGet();
//     // console.log('token: ', this.token);
//     if (this.token) {
//       this._contasServ.validarToken(this.token).subscribe(
//         (api: IApiResponse) => {
//           // console.log(api);
//           if (api.ok) {
//             if (api.data) {
//               this.conta = api.data;
//               console.log(this.conta);
//               // this.conta.con_fk_cam_dis_zap = 123;
//               // this.conta.con_fk_cam_dis_sms = 123;
//               this.numProds = 0;
//               if (this.conta.con_fk_cam_dis_zap) {
//                 this.numProds++;
//               } // if
//               if (this.conta.con_fk_cam_dis_sms) {
//                 this.numProds++;
//               } // if
//               // console.log(this.conta);
//               this.onClearClick();
//             } else {
//               this._cdStorageServ.removeStorageItem(KEY_DISPARO_TOKEN);
//               this._refresh();
//               // alert('Chave inválida.');
//             } // else
//           } else {
//             console.error(JSON.stringify(api.errors));
//           } // else
//         }
//       );
//     } // if