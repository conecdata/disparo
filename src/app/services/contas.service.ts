//#region ng
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region models
import { environment } from "../../environments/environment";
import { IConta } from "../modules/_submarine/_models/_interfaces";
import { IPagination, IEndereco } from "../modules/_conecdata/_models/_interfaces";
//#endregion

@Injectable({
    providedIn: "root"
})
export class ContasService {
    //#region constructor
    constructor(private _http: HttpClient) { }
    //#endregion

    //#region misc
    fix(row: any): IConta {
        row = row || {};
        let R: IConta = row;
        R.con_pk = parseInt(row.con_pk) || 0;
        R.con_fk_agendamento_ac = parseInt(row.con_fk_agendamento_ac) || 0;
        R.con_fk_cam_dis_sms = parseInt(row.con_fk_cam_dis_sms) || 0;
        R.con_fk_cam_dis_zap = parseInt(row.con_fk_cam_dis_zap) || 0;
        R.con_fk_campanha_ac = parseInt(row.con_fk_campanha_ac) || 0;
        R.con_fk_conta = parseInt(row.con_fk_conta) || 0;
        R.con_fk_endereco = parseInt(row.con_fk_endereco) || 0;
        R.con_b_ativo = row.con_b_ativo > 0;
        R.con_b_salvar_contatos_disparo = row.con_b_salvar_contatos_disparo > 0;

        return R;
    }

    fixes(rows: IConta[]): IConta[] {
        if (rows) {
            for (let row of rows) {
                row = this.fix(row);
            } // for
        } // if
        // console.log(rows);
        return rows || [];
    }

    img(licenca: string, big: boolean = false) {
        return big ? `${environment.url.img.conecdata}/clientes/big/${licenca}.jpg` : `${environment.url.img.conecdata}/clientes/${licenca}.jpg`;
    }

    relatorioContasRelacionadas(startDate: string, endDate: string, idConta = 0): Observable<any> {
        const INPUT = {
            startDate,
            endDate,
            idConta
        };
        // console.log(JSON.stringify(INPUT));
        const URL = encodeURI(`${environment.url.api.submarine.remote}/contas/relatorio`);
        return this._http.post(URL, INPUT);
    }
    //#endregion

    //#region methods C

    //#endregion

    //#region methods R
    /*
        'data' => '' (jwt)
    */
    registrar(chave: string): Observable<any> {
        const INPUT = { chave };
        const URL = encodeURI(`${environment.url.api.submarine.remote}/contas/disparo/registrar`);
        // console.log(`url: ${URL}`);
        return this._http.post(URL, INPUT);
        // const URL = "../../../../../assets/mock/submarine-contas-login.json";
        // return this._http.get(URL);
    }
    
    /*
        'data' => null 
    */
   validarToken(token: string): Observable<any> {
    const INPUT = { token };
    const URL = encodeURI(`${environment.url.api.submarine.remote}/contas/disparo/validar-token`);
    // console.log(`url: ${URL}`);
    return this._http.post(URL, INPUT);
    // const URL = "../../../../../assets/mock/submarine-contas-login.json";
    // return this._http.get(URL);
}

/* 
    /*
        'data' => null (IConta)
    * /
    conta(idConta: number = 0): Observable<any> {
        const INPUT = {
            idConta
        };
        const URL = encodeURI(
            `${environment.url.api.submarine.remote}/contas/id`
        );
        // const URL = "../../../../../assets/mock/formas-pgto.json";
        return this._http.post(URL, INPUT);
    }

    /*
        'data' => false (boolean)
    * /
    validateToken(idConta: number, token: string): Observable<any> {
        const INPUT = {
            idConta,
            token
        }
        const URL = encodeURI(
            `${environment.url.api.submarine.remote}/contas/validate-token`
        );
        // const URL = "../../../../../assets/mock/formas-pgto.json";
        return this._http.post(URL, INPUT);
    } 
*/
    //#endregion

    //#region methods U

    //#endregion

    //#region methods D

    //#endregion
}