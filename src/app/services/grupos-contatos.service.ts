//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region models
import { environment } from "../../environments/environment";
import { IGrupoContatos } from "../modules/_submarine/_models/_interfaces";
import { IPagination } from "../modules/_conecdata/_models/_interfaces";
//#endregion

@Injectable({
    providedIn: "root"
})
export class GruposContatosService {
    //#region constructor
    constructor(private _http: HttpClient) { }
    //#endregion

    //#region misc
    fix(row: any): IGrupoContatos {
        row = row || {};
        let R: IGrupoContatos = row;
        R.grc_pk = parseInt(row.grc_pk) || 0;
        R.grc_fk_conta = parseInt(row.grc_fk_conta) || 0;
        R.qtde_contatos = parseInt(row.qtde_contatos) || 0;

        return R;
    }

    fixes(rows: IGrupoContatos[]): IGrupoContatos[] {
        if (rows) {
            for (let row of rows) {
                row = this.fix(row);
            } // for
        } // if
        // console.log(rows);
        return rows || [];
    }
    //#endregion

    //#region methods C
    /*
        'data' => 0 (id)
    */
    add(nomeGrupo: string, idConta: number): Observable<any> {
        const INPUT = {
            nomeGrupo,
            idConta
        };
        // console.log(INPUT);
        const URL = encodeURI(`${environment.url.api.submarine.remote}/grupos-contatos/add`);
        // console.log(`url: ${URL}`);
        return this._http.post(URL, INPUT);
    }
    // //#endregion

    //#region methods R
    /*
    'data' => [
        'count' => 0,
        'rows' => null (IGrupoContatos[])
    ]
    */
    pag(pag: IPagination, idConta: number): Observable<any> {
        const INPUT = {
            pag,
            idConta
        };
        // console.log(INPUT);
        const URL = encodeURI(`${environment.url.api.submarine.remote}/grupos-contatos/pag`);
        return this._http.post(URL, INPUT);
    }

    /*
        'data' => null (IGrupoContatos[])
    */
    gruposToMove(idGrupo: number, idConta: number): Observable<any> {
        const INPUT = {
            idGrupo,
            idConta
        }
        const URL = encodeURI(
            `${environment.url.api.submarine.remote}/grupos-contatos/to-move`
        );
        return this._http.post(URL, INPUT);
    }

    /*
        'data' => null (IGrupoContatos[])
    */
    grupos(idConta: number): Observable<any> {
        const INPUT = {
            idConta
        }
        const URL = encodeURI(
            `${environment.url.api.submarine.remote}/grupos-contatos`
        );
        return this._http.post(URL, INPUT);
    }
    //#endregion

    //#region methods U
    rename(idGrupo: number, novoNome: string, idConta: number): Observable<any> {
        const INPUT = {
            idGrupo,
            novoNome,
            idConta
        }
        const URL = encodeURI(
            `${environment.url.api.submarine.remote}/grupos-contatos/rename`
        );
        return this._http.post(URL, INPUT);
    }

    // update(idContato: number, contato: IGrupoContatos, endereco: IEndereco) {
    //     contato.cnt_pk = idContato;
    //     if (endereco) {
    //         const CHARS = '-.';
    //         endereco.end_c_cep = endereco.end_c_cep.replace(new RegExp(`[${CHARS}]`, 'g'), '');
    //     }
    //     contato.cnt_c_fone = contato.cnt_c_fone.replace('(', '').replace(')', '').replace('-', '').replace(' ', '');
    //     const INPUT = {
    //         contato,
    //         endereco
    //     };
    //     // console.log(INPUT);
    //     const URL = encodeURI(`${environment.url.api.submarine.remote}/contatos/update`);
    //     // console.log(`url: ${URL}`);
    //     return this._http.post(URL, INPUT);
    // }
    // //#endregion

    // //#region methods D
    del(idGrupo: number) {
        const URL = encodeURI(
            `${environment.url.api.submarine.remote}/grupos-contatos/${idGrupo}`
        );
        // console.log(`url: ${URL}`);
        return this._http.delete(URL);
    }
    //#endregion
}