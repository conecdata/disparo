//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region models
import { environment } from "../../environments/environment";
import { IMensagem } from "../modules/_submarine/_models/_interfaces";
//#endregion

@Injectable({
    providedIn: "root"
})
export class MensagensService {
    //#region constructor
    constructor(private _http: HttpClient) { }
    //#endregion

    //#region misc
    fix(row: any): IMensagem {
        row = row || {};
        let R: IMensagem = row;
        R.msg_pk = parseInt(row.msg_pk) || 0;
        R.msg_fk_agendamento = parseInt(row.msg_fk_agendamento) || 0;
        R.msg_fk_campanha = parseInt(row.msg_fk_campanha) || 0;
        R.msg_fk_grupo_produtos = parseInt(row.msg_fk_grupo_produtos) || 0;
        R.msg_b_old = row.msg_b_old > 0;

        return R;
    }

    fixes(rows: IMensagem[]): IMensagem[] {
        if (rows) {
            for (let row of rows) {
                row = this.fix(row);
            } // for
        } // if
        // console.log(rows);
        return rows || [];
    }

    img(licenca: string = '', filename: string = ''): string {
        if (filename) {
            return licenca ?
                `${environment.url.img.submarine}/msgs/${licenca}/${filename}` :
                `${environment.url.img.tmp}/${filename}`;
        } else {
            return '';
        }
    }
    //#endregion

    //#region methods C
    /*
        'data' => 0 (id)
    */
    add(mensagem: IMensagem, idConta: number): Observable<any> {
        const INPUT = {
            mensagem,
            idConta
        };
        // console.log(INPUT);
        const URL = encodeURI(`${environment.url.api.submarine.remote}/mensagens/add`);
        // console.log(`url: ${URL}`);
        return this._http.post(URL, INPUT);
    }
    //#endregion

    //#region R
    buscaAgendamento(idAgendamento: number, idConta: number = 0): Observable<any> {
        const INPUT = {
            idAgendamento,
            idConta
        };
        // console.log(JSON.stringify(INPUT));
        const URL = encodeURI(`${environment.url.api.submarine.remote}/mensagens/busca-agendamento`);
        // console.log(`url: ${URL}`);
        return this._http.post(URL, INPUT);
        // const URL = "../../../../../assets/mock/submarine-movimentos-pag.json";
        // return this._http.get(URL);
    }

    buscaCampanha(idCampanha: number, idConta: number = 0, chaveDisparo: string = ''): Observable<any> {
        const INPUT = {
            idCampanha,
            idConta,
            chaveDisparo
        };
        // console.log(JSON.stringify(INPUT));
        const URL = encodeURI(`${environment.url.api.submarine.remote}/mensagens/busca-campanha`);
        // console.log(`url: ${URL}`);
        return this._http.post(URL, INPUT);
        // const URL = "../../../../../assets/mock/submarine-movimentos-pag.json";
        // return this._http.get(URL);
    }

    buscaId(idMsg: string, idConta: number = 0): Observable<any> {
        const INPUT = {
            idMsg,
            idConta
        };
        // console.log(JSON.stringify(INPUT));
        const URL = encodeURI(`${environment.url.api.submarine.remote}/mensagens/busca-id`);
        // console.log(`url: ${URL}`);
        return this._http.post(URL, INPUT);
        // const URL = "../../../../../assets/mock/submarine-movimentos-pag.json";
        // return this._http.get(URL);
    }
    //#endregion

    //#region methods U
    update(idMensagem: number, mensagem: IMensagem, idConta: number) {
        mensagem.msg_pk = idMensagem;
        const INPUT = {
            mensagem,
            idConta
        };
        // console.log(INPUT);
        const URL = encodeURI(`${environment.url.api.submarine.remote}/mensagens/update`);
        // console.log(`url: ${URL}`);
        return this._http.post(URL, INPUT);
    }
    //#endregion

    //#region methods D
    del(idMensagem: number) {
        const URL = encodeURI(
            `${environment.url.api.submarine.remote}/mensagens/${idMensagem}`
        );
        // console.log(`url: ${URL}`);
        return this._http.delete(URL);
    }
    //#endregion
}