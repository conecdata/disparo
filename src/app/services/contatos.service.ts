//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region models
import { environment } from "../../environments/environment";
import { IContato } from "../modules/_submarine/_models/_interfaces";
import { IPagination, IEndereco } from "../modules/_conecdata/_models/_interfaces";
//#endregion

@Injectable({
    providedIn: "root"
})
export class ContatosService {
    //#region constructor
    constructor(private _http: HttpClient) { }
    //#endregion

    //#region misc
    fix(row: any): IContato {
        row = row || {};
        let R: IContato = row;
        R.cnt_pk = parseInt(row.cnt_pk) || 0;
        R.cnt_fk_conta = parseInt(row.cnt_fk_conta) || 0;
        R.cnt_fk_endereco = parseInt(row.cnt_fk_endereco) || 0;
        R.cnt_fk_grupo = parseInt(row.cnt_fk_grupo) || 0;
        R.cnt_fk_ult_mov_age = parseInt(row.cnt_fk_ult_mov_age) || 0;
        R.cnt_b_ativo = row.cnt_b_ativo > 0;
        R.cnt_b_problema = row.cnt_b_problema > 0;
        R.cnt_d_nascimento = row.cnt_d_nascimento || null;
        R.sel = row.sel > 0;

        return R;
    }

    fixes(rows: IContato[]): IContato[] {
        if (rows) {
            for (let row of rows) {
                row = this.fix(row);
            } // for
        } // if
        // console.log(rows);
        return rows || [];
    }
    //#endregion

    //#region methods C
    /*
        'data' => 0 (id)
    */
    add(contato: IContato, endereco: IEndereco): Observable<any> {
        if (endereco) {
            const CHARS = '-.';
            endereco.end_c_cep = endereco.end_c_cep.replace(new RegExp(`[${CHARS}]`, 'g'), '');
        }
        contato.cnt_c_fone = contato.cnt_c_fone.replace('(', '').replace(')', '').replace('-', '').replace(' ', '');
        const INPUT: {
            contato: IContato,
            endereco: IEndereco;
        } = {
            contato,
            endereco
        };
        // console.log(INPUT);
        const URL = encodeURI(`${environment.url.api.submarine.remote}/contatos/add`);
        // console.log(`url: ${URL}`);
        return this._http.post(URL, INPUT);
    }

    subUploadCsvFile(csv: File, idConta: number): Observable<any> {
        const INPUT = new FormData();
        INPUT.append('file', csv, csv.name);
        const URL = encodeURI(`${environment.url.api.submarine.remote}/contatos/upload-csv/conta/${idConta}`);
        // console.log(`url: ${URL}`);
        return this._http.post(URL, INPUT, {
            reportProgress: true,
            observe: 'events'
        });
    }
    //#endregion

    //#region methods R
    /*
    'data' => [
        'count' => 0,
        'rows' => null (IContato[])
    ]
    */
    pag(pag: IPagination, idConta: number): Observable<any> {
        const INPUT = {
            pag,
            idConta
        };
        // console.log(INPUT);
        const URL = encodeURI(`${environment.url.api.submarine.remote}/contatos/pag`);
        return this._http.post(URL, INPUT);
        // const URL = "../../../../../assets/mock/submarine-contas-pag.json";
        // return this._http.get(URL);
    }

    /*
        'data' => null (IContato[])
    */
    contatos(filter: string, idConta: number): Observable<any> {
        const INPUT = {
            filter,
            idConta
        }
        const URL = encodeURI(
            `${environment.url.api.submarine.remote}/contatos`
        );
        return this._http.post(URL, INPUT);
    }

    /*
        'data' => null (IContato[])
    */
    contatosGrupo(idGrupo: number, idConta: number): Observable<any> {
        const INPUT = {
            idGrupo,
            idConta
        }
        const URL = encodeURI(
            `${environment.url.api.submarine.remote}/contatos/grupo`
        );
        return this._http.post(URL, INPUT);
    }

    /*
        'data' => null (any)
        {
            total: 0,
            ativos: 0,
            problemas: 0,
            auto_sel: 0
        }
    */
    mapaTipos(idConta: number): Observable<any> {
        const INPUT = {
            idConta
        }
        const URL = encodeURI(
            `${environment.url.api.submarine.remote}/contatos/mapa-tipos`
        );
        return this._http.post(URL, INPUT);
    }
    //#endregion

    //#region methods U
    update(idContato: number, contato: IContato, endereco: IEndereco) {
        contato.cnt_pk = idContato;
        if (endereco) {
            const CHARS = '-.';
            endereco.end_c_cep = endereco.end_c_cep.replace(new RegExp(`[${CHARS}]`, 'g'), '');
        }
        contato.cnt_c_fone = contato.cnt_c_fone.replace('(', '').replace(')', '').replace('-', '').replace(' ', '');
        const INPUT = {
            contato,
            endereco
        };
        // console.log(INPUT);
        const URL = encodeURI(`${environment.url.api.submarine.remote}/contatos/update`);
        // console.log(`url: ${URL}`);
        return this._http.post(URL, INPUT);
    }

    contatosMove(idGrupo: number, idsContatos: number[], novoGrupo: number, idConta: number): Observable<any> {
        const INPUT = {
            idGrupo,
            idsContatos,
            novoGrupo,
            idConta
        }
        const URL = encodeURI(
            `${environment.url.api.submarine.remote}/contatos/move`
        );
        return this._http.post(URL, INPUT);
    }
    //#endregion

    //#region methods D
    del(idContato: number) {
        const URL = encodeURI(
            `${environment.url.api.submarine.remote}/contatos/${idContato}`
        );
        // console.log(`url: ${URL}`);
        return this._http.delete(URL);
    }
    //#endregion
}