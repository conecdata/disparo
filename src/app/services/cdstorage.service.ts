//#region ng
import { Injectable } from '@angular/core';
//#endregion

//#region capacitor
import { Plugins } from '@capacitor/core';
//#endregion

//#region app models
const { Storage } = Plugins;
import {
  KEY_DISPARO_TOKEN
} from "../modules/_submarine/_models/consts";
//#endregion

@Injectable({
  providedIn: 'root'
})
export class CdStorageService {
  //#region constructor
  constructor() { }
  //#endregion

  //#region functions
  // Store the value
  async store(storageKey: string, value: any) {
    const encryptedValue = btoa(escape(JSON.stringify(value)));
    await Storage.set({
      key: storageKey,
      value: encryptedValue
    });
  }

  // Get the value
  async get(storageKey: string, onSuccess?: any) {
    // console.log(storageKey);
    const ret = await Storage.get({ key: storageKey });
    if (onSuccess && typeof onSuccess === "function") {
      onSuccess(JSON.parse(unescape(atob(ret.value))));
    } // if
    // return JSON.parse(unescape(atob(ret.value)));
  }

  async getItem() {
    const value = await Storage.get({ key: 'name' });
    console.log('Got item: ', value);
  }

  async removeStorageItem(storageKey: string) {
    await Storage.remove({ key: storageKey });
  }

  // Clear storage
  async clear() {
    await Storage.clear();
  }
  //#endregion
}

  // tokenGet(storageKey: any, onSuccess?: any) {
  //   Storage.get(storageKey)
  //     .then(
  //       (val: any) => {
  //       if (onSuccess && typeof onSuccess === "function") {
  //         onSuccess(val);
  //       } // if
  //     })
  //     .catch(err => {
  //       console.error(err);
  //     });
  // }