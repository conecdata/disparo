//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app models
// import { IRede } from "../models/interfaces";
import { environment } from "../../environments/environment";
// import { KEY_AUTH_TOKEN } from "../_shared/_models/consts";
//#endregion

@Injectable({
  providedIn: "root"
})
export class SubmarineService {
  //#region constructor
  constructor(private _http: HttpClient) { }
  //#endregion

  //#region methods R
  /*
    'data' => null (IAuthToken)
  */
  tokenData(token: string): Observable<any> {
    // console.log(token);
    const URL = encodeURI(
      `${environment.url.api.submarine.remote}/jwt-data/${token}`
    );
    // console.log(`url: ${URL}`);
    return this._http.get(URL);
  }
  //#endregion
}
