//#region ng
import { Injectable } from '@angular/core';
//#endregion

//#region app models
import {
  KEY_DISPARO_TOKEN
} from "../modules/_submarine/_models/consts";
//#endregion

//#region app services
import { StorageService as CdStorageService } from "../modules/_conecdata/_core/_services";
//#endregion

@Injectable({
  providedIn: "root"
})
export class StorageService {

  //#region constructor
  constructor(private _storageServ: CdStorageService) { }
  //#endregion

  //#region disparoToken
  private _disparoTokenDefs(): string {
    return '';
  }

  public disparoTokenGet(): string {
    return this._storageServ.ls_get(KEY_DISPARO_TOKEN, this._disparoTokenDefs());
  }

  public disparoTokenSet(token: string): string {
    if (token) {
      this._storageServ.ls_set(KEY_DISPARO_TOKEN, token);
    }; // if
    return this.disparoTokenGet();
  }

  public disparoTokenClear(): string {
    this._storageServ.ls_set(KEY_DISPARO_TOKEN, this._disparoTokenDefs());
    return this.disparoTokenGet();
  }
  //#endregion
}
