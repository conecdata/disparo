//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region models
import { environment } from "../../environments/environment";
import { ICampanha, IItemPesquisa } from "../modules/_submarine/_models/_interfaces";
import { IPagination } from "../modules/_conecdata/_models/_interfaces";
//#endregion

@Injectable({
    providedIn: "root"
})
export class CampanhasService {
    //#region constructor
    constructor(private _http: HttpClient) { }
    //#endregion

    //#region misc
    fix(row: any): ICampanha {
        row = row || {};
        let R: ICampanha = row;
        R.cam_pk = parseInt(row.cam_pk) || 0;
        R.cam_fk_conta = parseInt(row.cam_fk_conta) || 0;
        R.cam_fk_produto = parseInt(row.cam_fk_produto) || 0;
        R.cam_fk_tipo_produto = parseInt(row.cam_fk_tipo_produto) || 0;
        R.cam_b_alerta1 = row.cam_b_alerta1 > 0;
        R.cam_b_alerta2 = row.cam_b_alerta2 > 0;
        R.cam_b_ilimitado = row.cam_b_ilimitado > 0;
        R.cam_dt_criado_em = row.cam_dt_criado_em ? new Date(row.cam_dt_criado_em) : null;
        R.cam_f_valor = parseFloat(row.cam_f_valor) || 0;
        R.cam_f_perc_alerta1 = parseInt(row.cam_f_perc_alerta1) || 0;
        R.cam_f_perc_alerta2 = parseInt(row.cam_f_perc_alerta2) || 0;
        R.cam_i_conta = parseInt(row.cam_i_conta) || 0;
        R.perc_disponivel = parseFloat(row.perc_disponivel) || 0;
        R.qtde_agendamentos = parseInt(row.qtde_agendamentos) || 0;
        R.qtde_movimentos = parseInt(row.qtde_movimentos) || 0;
        R.qtde_itens_pesquisa = parseInt(row.qtde_itens_pesquisa) || 0;

        return R;
    }

    fixes(rows: ICampanha[]): ICampanha[] {
        if (rows) {
            for (let row of rows) {
                row = this.fix(row);
            } // for
        } // if
        // console.log(rows);
        return rows || [];
    }
    //#endregion

    //#region methods C
    /*
        'data' => 0 (id)
    */
    add(idConta: number, campanha: ICampanha, movimentosCampanhas: any, itensPesquisa: IItemPesquisa[], idSaldo: number = 0): Observable<any> {
        campanha.cam_fk_conta = idConta;
        const INPUT: {
            campanha: ICampanha,
            movimentosCampanhas: any,
            itensPesquisa: IItemPesquisa[],
            idSaldo: number;
        } = {
            campanha,
            movimentosCampanhas,
            itensPesquisa,
            idSaldo
        };
        // console.log(INPUT);
        const URL = encodeURI(`${environment.url.api.submarine.remote}/campanhas/add`);
        // console.log(`url: ${URL}`);
        return this._http.post(URL, INPUT);
    }
    //#endregion

    //#region R
    /*
    'data' => [
        'count' => 0,
        'rows' => null (ICampanha[])
    ]
    */
    pag(pag: IPagination, idConta: number): Observable<any> {
        const INPUT = {
            pag,
            idConta
        };
        // console.log(INPUT);
        const URL = encodeURI(`${environment.url.api.submarine.remote}/campanhas/pag`);
        // console.log(`url: ${URL}`);
        return this._http.post(URL, INPUT);
        // const URL = "../../../../../assets/mock/submarine-campanhas-pag.json";
        // return this._http.get(URL);
    }

    /*
    'data' => [
        'count' => 0,
        'rows' => null (ICampanha[])
    ]
    */
    pagCdman(pag: IPagination): Observable<any> {
        const INPUT = {
            pag
        };
        // console.log(INPUT);
        const URL = encodeURI(`${environment.url.api.submarine.remote}/campanhas/cdman/pag`);
        // console.log(`url: ${URL}`);
        return this._http.post(URL, INPUT);
        // const URL = "../../../../../assets/mock/submarine-campanhas-pag.json";
        // return this._http.get(URL);
    }

    /*
    'data' => null (ICampanha[])
    */
    campanhas(idConta: number): Observable<any> {
        const INPUT = {
            idConta
        };
        const URL = encodeURI(`${environment.url.api.submarine.remote}/campanhas`);
        // console.log(`url: ${URL}`);
        return this._http.post(URL, INPUT);
    }

    /*
    'data' => null (ICampanha[])
    */
    campanhasTipoProduto(idTipoProduto: number, idConta: number): Observable<any> {
        const INPUT = {
            idTipoProduto,
            idConta
        };
        const URL = encodeURI(`${environment.url.api.submarine.remote}/campanhas/tipo-produto`);
        // console.log(`url: ${URL}`);
        return this._http.post(URL, INPUT);
    }

    /*
    'data' => null (ICampanha[])
    */
    campanhasGrupoProduto(idGrupoProduto: number, idConta: number): Observable<any> {
        const INPUT = {
            idGrupoProduto,
            idConta
        };
        const URL = encodeURI(`${environment.url.api.submarine.remote}/campanhas/grupo-produto`);
        // console.log(`url: ${URL}`);
        return this._http.post(URL, INPUT);
    }

    /*
        'data' => null (IConta)
    */
    campanha(idCampanha: number): Observable<any> {
        const INPUT = {
            idCampanha
        };
        const URL = encodeURI(
            `${environment.url.api.submarine.remote}/campanhas/id`
        );
        // const URL = "../../../../../assets/mock/formas-pgto.json";
        return this._http.post(URL, INPUT);
    }
    //#endregion

    //#region methods U
    updateCdman(idCampanha: number, idConta: number, campanha: ICampanha, itensPesquisa: IItemPesquisa[]) {
        campanha.cam_pk = idCampanha;
        campanha.cam_fk_conta = idConta;
        const INPUT = {
            campanha,
            itensPesquisa
        };
        // console.log(INPUT);
        const URL = encodeURI(`${environment.url.api.submarine.remote}/campanhas/cdman/update`);
        // console.log(`url: ${URL}`);
        return this._http.post(URL, INPUT);
    }

    update(idCampanha: number, campanha: ICampanha, itensPesquisa: IItemPesquisa[]) {
        campanha.cam_pk = idCampanha;
        const INPUT = {
            campanha,
            itensPesquisa
        };
        // console.log(INPUT);
        const URL = encodeURI(`${environment.url.api.submarine.remote}/campanhas/update`);
        // console.log(`url: ${URL}`);
        return this._http.post(URL, INPUT);
    }
    //#endregion
}