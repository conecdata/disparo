//#region ng
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region models
import { environment } from "../../environments/environment";
//#endregion

@Injectable({
    providedIn: "root"
})
export class ApiService {
    //#region constructor
    constructor(private _http: HttpClient) { }
    //#endregion

    //#region methods SMS
    send(apiToken: string, to: string, msgs: any[]): Observable<any> {
        let mensagens: any[] = [];
        msgs.forEach(e => {
            mensagens.push(
                {
                    msg: e.msg_c_msg,
                    arq: e.msg_c_arq,
                    extra: e.msg_c_extra
                }
            )
        });
        console.log(mensagens)
        const INPUT = {
            // apiToken,
            to,
            msgs: mensagens
        };
        console.log(INPUT);
        var header = {
            headers: new HttpHeaders()
                .set('Authorization', `Bearer ${apiToken}`)
        };
        const URL = encodeURI(`${environment.url.api.submarine.remote}/api/send`);
        // console.log(`url: ${URL}`);
        return this._http.post(URL, INPUT, header);
    }

    // send(infoData: any): Observable<any> {
    //     infoData.to = infoData.to.replace('(', '').replace(')', '').replace('-', '').replace(' ', '');
    //     infoData.to = [infoData.to];
    //     // console.log(infoData);
    //     const URL = encodeURI(`${environment.url.api.submarine.remote}/api/send-multi`);
    //     // console.log(`url: ${URL}`);
    //     return this._http.post(URL, infoData);
    // }

    // send_multi_contatos(infoData: any): Observable<any> {
    //     // console.log(INPUT);
    //     const URL = encodeURI(`${environment.url.api.submarine.remote}/api/send-multi-contatos`);
    //     // console.log(`url: ${URL}`);
    //     return this._http.post(URL, infoData);
    // }
    //#endregion
}