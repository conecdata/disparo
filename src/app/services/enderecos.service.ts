//#region ng
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
//#endregion

//#region 3rd
import { Observable } from "rxjs";
//#endregion

//#region app models
import { IEndereco } from "../modules/_conecdata/_models/_interfaces";
import { environment } from "../../environments/environment";
// import { KEY_AUTH_TOKEN } from "../_shared/_models/consts";
//#endregion

@Injectable({
  providedIn: "root"
})
export class EnderecosService {
  //#region constructor
  constructor(private _http: HttpClient) { }
  //#endregion

  //#region misc
  fix(row: any): IEndereco {
    row = row || {};
    let R: IEndereco = row;
    R.end_pk = parseInt(row.end_pk) || 0;
    R.end_i_nro = parseInt(row.end_i_nro) || 0;

    return R;
  }

  fixes(rows: any[]): IEndereco[] {
    if (rows) {
      for (let row of rows) {
        row = this.fix(row);
      } // for
    } // if
    // console.log(rows);
    return rows || [];
  }
  //#endregion

  //#region methods R
  endereco(id: number): Observable<any> {
    const URL = encodeURI(`${environment.url.api.submarine.remote}/enderecos/id/${id}`);
    return this._http.get(URL);
  }
  //#endregion
}
